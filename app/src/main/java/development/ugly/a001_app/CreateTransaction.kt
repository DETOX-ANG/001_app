package development.ugly.a001_app

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.EditText
import development.ugly.a001_app.Distribution_Style.*
import development.ugly.a001_app.database.DB_Helper
import development.ugly.a001_app.database.DB_Interfaces
import development.ugly.a001_app.database.Transaction_table.*
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.main_classes.Transaction_Fin
import kotlinx.android.synthetic.main.create_transaction_activity.*
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.Toast
import development.ugly.a001_app.cls_Interfaces.IAuxiliary
import development.ugly.a001_app.database.Accounts_table.loadAccounts
import development.ugly.a001_app.database.Category_table.Category_table

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateTransaction : AppCompatActivity(), IAuxiliary {



    lateinit var date: Calendar
    lateinit var sdt: SimpleDateFormat
    var i = 1
    lateinit var trans_db : Transaction_DB
    lateinit var cat_db : Category_table

  //  var transFrag = MenuFragsAdapter(supportFragmentManager)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_transaction_activity)
        sdt = SimpleDateFormat("dd/MM/yyyy")
        date = Calendar.getInstance()
        cat_db = Category_table(this)
        trans_db = Transaction_DB(this)
      //Pre-Load Advanced Options Fragment
        preloadSpinners()

        Trans_Date_TXT.setOnTouchListener { v, event -> showDatePicker(event)}

        dist_group.setOnCheckedChangeListener {group, checkedId ->radioButtonListener(checkedId)}
        }

    private fun showDatePicker(event: MotionEvent): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                datePick(this, Trans_Date_TXT)
                hideKeyboard(this, Trans_Date_TXT)
            }
            else -> hideKeyboard(this, Trans_Date_TXT)
        }
    }

    // TODO melhorar com um try catch
    private fun radioButtonListener(checkedId: Int) {
        when (checkedId) {
            R.id.SplitValue_Radio -> splitValuesCalc()
            R.id.FixedValue_Radio -> fixedValuesCalc()
        }
    }

    private fun fixedValuesCalc() {
        var totalByMonthv : Double
        Trans_rpt.text = "Total: "
        if (TransValue_TXT.text.isEmpty()){
            totalByMonthv = 0.00}
        else {
            totalByMonthv = returnTransactionValue() * rpt_end_TEXT.text.toString().toInt()
        }
        Trans_rpt_LBL.text = totalByMonthv.toString()
    }

    private fun splitValuesCalc() {
        var totalByMonthv : Double
        Trans_rpt.text = "Value by\n Month"
        if (TransValue_TXT.text.isEmpty()){ totalByMonthv = 0.00}
        else{ totalByMonthv = returnTransactionValue() / rpt_end_TEXT.text.toString().toInt()
        }
        Trans_rpt_LBL.text = totalByMonthv.toString()
    }

    fun showARepeat(view: View) {
        when (Repeat_switch.isChecked) {
            true -> Repeat_layout.visibility = View.VISIBLE
            false -> Repeat_layout.visibility = View.GONE}}

    fun startSaveProcess(view: View) {
        var ss =createInstallments()

        if (preSaveValidation(mutableListOf(TransValue_TXT, Trans_Desc_TXT, Trans_Date_TXT))) {
           trans_db.saveTransDetails(ss)
            finish()
        }
        else{
            //TODO melhorar mensagem
            Toast.makeText(this,"Fail",Toast.LENGTH_LONG).show()
        }
    }

    fun preloadSpinners(){
    Trans_Acct_Spinner.adapter = ArrayAdapter<Account_fin>(this,android.R.layout.simple_list_item_1, loadAccounts(this))
    Trans_Cat_spinner.adapter  = ArrayAdapter<Categories>(this,android.R.layout.simple_list_item_1, cat_db.loadcategories())
    }

    fun IsAdvancedChecked(view: View){
    super.onCheckBoxVisibilityChange(Trans_Adv_CheckBox, Repeat_Dtl)
    }

    fun returnTransactionType(): Int{
        when(Trans_clsf.selectedTabPosition){
            0->return +1
            else ->  return -1
        }
     }

    //TODO Validate fields before saving to avoid application closing
    //Category, Bank, Description, Date, amount
    fun preSaveValidation(fields: MutableList<EditText>):Boolean{
        var j = 0
        var status: Boolean
        do {
            if (!fields[j].text.isEmpty()){
                status = true
                j++
            }
            else {
                status = false
            j = fields.size
            }
        }
        while (j != fields.size)
        return status
    }

    fun returnTransactionValue():Double{
    if (TransValue_TXT.text.isEmpty())
    {
        return 0.00
    }
    else
    {
        return TransValue_TXT.text.toString().toDouble()
    }
    }

    fun createInstallments(): ArrayList<Transaction_Fin>{
    var installmentList = ArrayList<Transaction_Fin>()
    date.time = sdt.parse(Trans_Date_TXT.text.toString())
    var catid = Trans_Cat_spinner.selectedItem as Categories
    var acctId = Trans_Acct_Spinner.selectedItem as Account_fin
    var amtValue =     CalculateInstallments()*returnTransactionType()

    var qqt = returnInstallmentsCount()
        do{
            installmentList.add(
                Transaction_Fin(trans_db.returnNextValue(),
                    Trans_Desc_TXT.text.toString(),
                    amtValue,
                    catid,
                    sdt.format(date.time),
                    acctId,
                    Trans_Date_TXT.text.toString(),
                    "N",
                    i++
                    )
            )

           date.add(Calendar.MONTH,1)
            qqt--
        }
        while (qqt != 0)
    return installmentList
    }


fun CalculateInstallments(): Double{
        when (dist_group.checkedRadioButtonId)
        {R.id.FixedValue_Radio ->return TransValue_TXT.text.toString().toDouble()
            R.id.SplitValue_Radio -> return TransValue_TXT.text.toString().toDouble() / rpt_end_TEXT.text.toString().toDouble()
            else -> return 0.00}
}



    fun returnInstallmentsCount(): Int{
        if (!Repeat_switch.isChecked)
        {
           return 1
        }
        else{
            return rpt_end_TEXT.text.toString().toInt()
        }
    }

//    Transaction_Fin(id,CalculateInstallments()*returnTransactionType(),sdt.format(date.time),catid,acctId,i++ )
//    override fun returnNextValue(): Int? {
//    var transaction_Fin = Transaction_Fin()
//    db = DB_Helper(this).writableDatabase
//    var cursor = db.rawQuery("SELECT MAX($ftrans_col_ID) AS $ftrans_col_ID FROM $ftrans_table_name",null)
//    if (cursor.moveToFirst()){
//    do {transaction_Fin.trans_fin_id = cursor.getInt(cursor.getColumnIndex(ftrans_col_ID))+1}
//    while (cursor.moveToNext())}
//    return transaction_Fin.trans_fin_id}


//    fun returnDistStyle(): Distribution_Style {
//        val distribution_Style = when (dist_group.checkedRadioButtonId) {
//            R.id.FixedValue_Radio -> FIXED
//            R.id.SplitValue_Radio -> SPLIT
//            else -> { Null }
//        }
//        return  distribution_Style
//    }

//    fun setTransactionValues():Transaction_Fin{
//        var catid = Trans_Cat_spinner.selectedItem as Categories
//        var acctId = Trans_Acct_Spinner.selectedItem as Account_fin
//        var id = returnNextValue()
//        var  transaction_F = Transaction_Fin()
//        transaction_F.trans_fin_id = returnNextValue()
//        transaction_F.transdescription = Trans_Desc_TXT.text.toString()
//        transaction_F.amount = TransValue_TXT.text.toString().toDouble()*returnTransactionType()
//        transaction_F.trans_date = sdt.format(date.time)
//        transaction_F.categories.cat_id = catid.cat_id
//        transaction_F.account_fin.acct_id = acctId.acct_id
//        if (!Repeat_switch.isChecked)
//        {
//            transaction_F.installSeq = 1
//        }
//        else transaction_F.installSeq = rpt_end_TEXT.text.toString().toInt()
//        return transaction_F }


}
