package development.ugly.a001_app.main_classes.Accounts

class CreditCard_Account : Account_fin{

    var CC_ID: Int? = null
    var CC_Limit: Double = 0.00

    var CC_Close_day: Int? = null


    constructor(acct_desc: String, CC_Limit: Double, CC_Close_day: Int?) : super(acct_desc) {
        this.CC_Limit = CC_Limit
        this.CC_Close_day = CC_Close_day
    }


}