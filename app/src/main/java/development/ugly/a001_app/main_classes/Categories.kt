package development.ugly.a001_app.main_classes

class Categories {

    var cat_id: Int? = null
    lateinit var cat_description: String
    var cat_parent: String? = null

    constructor(cat_id: Int?, cat_description: String, cat_parent: String?) {
        this.cat_id = cat_id
        this.cat_description = cat_description
        this.cat_parent = cat_parent
    }

    override fun toString(): String {
        return cat_description
    }

    constructor()


}
