package development.ugly.a001_app.main_classes

import development.ugly.a001_app.Distribution_Style
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.simpleDateFormat
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class Transaction_Fin : Serializable {

    //TODO Create possible methods to support other classes
    var trans_fin_id: Int? = null
    lateinit var transdescription: String
    var amount: Double = 0.00
    lateinit var category: String
    lateinit var account_transaction: String
    lateinit var trans_date: String
    var installments: Boolean = false
   var distribution_Style : Distribution_Style = Distribution_Style.Null
    lateinit var categories: Categories
    lateinit var account_fin: Account_fin
    var installSeq: Int = 0
    lateinit var status: String
    lateinit var paidAmt: String
    var date: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
    lateinit var sdate: Calendar
    lateinit var tdate: Calendar
    lateinit var pdate: Calendar


    constructor(transID: Int?,
                transdescription: String,
                amount: Double,
                categories: Categories,
                dueDate : String,
                account_fin: Account_fin,
                trans_date: String,
                payStatus: String,
                installSeq: Int
                )
    {
        this.trans_fin_id = transID
        this.transdescription = transdescription
        this.amount = amount
        this.categories = categories
        this.sdate = Calendar.getInstance()
        this.trans_date = dueDate
        this.sdate.time = date.parse(dueDate)
        this.tdate = Calendar.getInstance()
        this.tdate.time = date.parse(trans_date)
        this.account_fin = account_fin
        this.status = payStatus
        this.installSeq = installSeq
        this.pdate = Calendar.getInstance()

    }


    constructor(
        transdescription: String,
        amount: Double,
        trans_date: String,
        installments: Boolean,
        category: String,
        account_fin: String,
        distribution_Style: Distribution_Style,
        date : Date
    ) {
        this.transdescription = transdescription
        this.amount = amount
        this.trans_date = trans_date
        this.installments = installments
        this.account_transaction = account_fin
        this.category = category
        this.distribution_Style = distribution_Style

    }


    constructor(){
        categories = Categories()
        account_fin = Account_fin()
    }

    constructor(transdescription: String, amount: Double, category: String) {
        this.transdescription = transdescription
        this.amount = amount
        this.category = category
    }

    constructor(
        transID: Int ,transdescription: String, amount: Double, category: String, account_transaction: String, trans_date: String, installSeq: Int
    ) {
        this.transdescription = transdescription
        this.amount = amount
        this.category = category
        this.account_transaction = account_transaction
        this.trans_date = trans_date
        this.installSeq = installSeq
        sdate = Calendar.getInstance()
        this.sdate.time = this.date.parse(trans_date)
        this.trans_fin_id = transID
    }

    constructor(
        transdescription: String, amount: Double, category: String, account_transaction: String, trans_date: String
    ) {
        this.transdescription = transdescription
        this.amount = amount
        this.category = category
        this.account_transaction = account_transaction
        this.trans_date = trans_date

    }

    constructor(
        trans_fin_id: Int?,
        amount: Double,
        trans_date: String,
        categories: Categories,
        account_fin: Account_fin,
        installSeq: Int
    ) {
        this.trans_fin_id = trans_fin_id
        this.amount = amount
        this.trans_date = trans_date
        this.categories = categories
        this.account_fin = account_fin
        this.installSeq = installSeq
    }
//    constructor(
//        transdescription: String, amount: Double, category: String, account_transaction: String, trans_date: String, installSeq: Int
//    ) {
//        this.transdescription = transdescription
//        this.amount = amount
//        this.category = category
//        this.account_transaction = account_transaction
//        this.trans_date = trans_date
//        this.installSeq = installSeq
//
//
//    }




}