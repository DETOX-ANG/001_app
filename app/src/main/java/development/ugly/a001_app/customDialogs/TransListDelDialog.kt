package development.ugly.a001_app.customDialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import development.ugly.a001_app.R
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.main_classes.Transaction_Fin
import kotlinx.android.synthetic.main.trans_list_del_dialog.*

class TransListDelDialog(): DialogFragment(){

    lateinit var transaction_db :Transaction_DB
    lateinit var transactionFin: Transaction_Fin
    var i : Int = 0
    var bundle = Bundle()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

    transactionFin = this.arguments!!["TRANSID"] as Transaction_Fin

    return  inflater.inflate(R.layout.trans_list_del_dialog,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    transaction_db = Transaction_DB(context)

    Del_Current.setOnClickListener {transaction_db.deleteTranction(transactionFin)}

    Del_Future.setOnClickListener { i = transaction_db.deleteFutureTranction(transactionFin)

    }

    Del_Full.setOnClickListener {transaction_db.deleteFullTranction(transactionFin)}

    super.onViewCreated(view, savedInstanceState)
    }


}