package development.ugly.a001_app.customDialogs

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.service.autofill.Sanitizer
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*
import development.ugly.a001_app.*
import development.ugly.a001_app.main_classes.Transaction_Fin
import kotlinx.android.synthetic.main.dialog_filter.*
import kotlinx.android.synthetic.main.transaction_list_activity.*
import java.text.DateFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.Month
import java.util.*
import java.util.Calendar.*
import kotlin.collections.ArrayList

class DatePicker_Custom: DialogFragment(){

    lateinit var button: Button
    lateinit var list: RecyclerView
    lateinit var simpleDateFormat: SimpleDateFormat


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        simpleDateFormat = SimpleDateFormat("MM/YYYY")
        return inflater.inflate(R.layout.dialog_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    MonYearFilterWheel(monthFilter,yearFilter)

    //TODO refactor code to improve performance and security
    Ok_Btn.setOnClickListener {
//        button = activity!!.findViewById(R.id.DateFilter_BTN)
        list = activity!!.findViewById(R.id.ListTransaction)

        date.month = monthFilter.value-1
        var time: String = simpleDateFormat.format(date.time)

        myfilterMonth = time
        button.text = myfilterMonth
//        setFragment(Transactions_List_frag(),R.id.transList_frame, loadInstallments(context).filter { transaction_Fin -> transaction_Fin.trans_date.contains(myfilterMonth) })
        dismiss()
    }

    Cancel_Btn.setOnClickListener { dismiss() }
    }

}