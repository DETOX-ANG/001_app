package development.ugly.a001_app.customDialogs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import development.ugly.a001_app.R
import development.ugly.a001_app.database.Category_table.Category_table
import development.ugly.a001_app.database.DB_Helper
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.onCheckBoxVisibilityChange
import kotlinx.android.synthetic.main.category__setup_activity.*


class Category_Defn_Custom : DialogFragment() {

    lateinit var categories: Categories
    lateinit var catTransaction_DB: Category_table

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.category__setup_activity, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        catTransaction_DB = Category_table(context)

        Cat_Save_BTN.setOnClickListener { saveCategories() }
        //CategoryParent_spin.adapter = getCategories(context)

        //Set Listerners
        //nested_check.setOnClickListener {onCheckBoxVisibilityChange(nested_check,nestLayout)}

        super.onViewCreated(view, savedInstanceState)
    }


    fun saveCategories() {
        categories = Categories()
        categories.cat_description = category_desc_TXT.text.toString()
        categories.cat_parent = IsNestChecked()
        catTransaction_DB.InsertCategory(categories)
    }

    fun IsNestChecked(): String? {
        if (nested_check.isChecked) {
            categories.cat_parent = CategoryParent_spin.selectedItem?.toString()
            return categories.cat_parent}
        else{
            categories.cat_parent = null
            return categories.cat_parent}}
}