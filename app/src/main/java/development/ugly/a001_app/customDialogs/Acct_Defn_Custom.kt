package development.ugly.a001_app.customDialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import development.ugly.a001_app.R
import development.ugly.a001_app.database.Accounts_table.createAccount
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import kotlinx.android.synthetic.main.bank_acct_frag.*

class Acct_Defn_Custom: DialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return  inflater.inflate(R.layout.bank_acct_frag,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        Acct_Save_bt.setOnClickListener { SaveAcct() }

        super.onViewCreated(view, savedInstanceState)
    }

    fun SaveAcct(){
        var account_fin = Account_fin()
        account_fin.acct_desc = bankAcctName_TXT.text.toString()
        account_fin.acct_bal = InitialBalance_txt.text.toString().toDouble()
        account_fin.accot_type = acct_Category_spin.selectedItem.toString()
        if (createAccount(context, account_fin))
        dismiss()
        else
        Toast.makeText(context,"Conta já existe",Toast.LENGTH_LONG).show()
    }
}