package development.ugly.a001_app

import android.graphics.drawable.GradientDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.*
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.recyclerview.selection.*
import development.ugly.a001_app.customAdapters.MenuFragsAdapter
import development.ugly.a001_app.customAdapters.TransListAdapter
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.main_classes.Transaction_Fin
import development.ugly.a001_app.recyclerView_custom.ItemDetail
import kotlinx.android.synthetic.main.goals_activity.*
import kotlinx.android.synthetic.main.transactions__list_frag.*
import android.view.MenuItem as MenuItem1

class Goals : AppCompatActivity() {


    var selectionTracker: SelectionTracker<Long>? = null
    lateinit var adapter: TransListAdapter
    var edit_menu: MenuItem1? = null
    var delete_menu: MenuItem1? = null
    var selectedItem: Long? = null
    lateinit var transactionDb: Transaction_DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.goals_activity)

        transactionDb = Transaction_DB(this)
        var load = transactionDb.loadInstallments()
        adapter = TransListAdapter(this, load, { Toast.makeText(this,"item clicado", Toast.LENGTH_LONG).show()})

        if (savedInstanceState != null){
            selectionTracker?.onRestoreInstanceState(savedInstanceState)
        }

       goalsList.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))

       goalsList.adapter =  adapter

       selectionTracker = SelectionTracker.Builder("aa1",
            goalsList,
            StableIdKeyProvider(goalsList),
            ItemDetail(goalsList),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate( SelectionPredicates.createSelectSingleAnything()).build()
        setSupportActionBar(Goals_ToolBar)

        adapter.selectionTracker = selectionTracker


        selectionTracker?.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                super.onSelectionChanged()
                var item = selectionTracker!!.selection.size()
                if (item > 0)
                {
                    selectedItem = selectionTracker?.selection?.first()
                    edit_menu?.isVisible = true
                    delete_menu?.isVisible = true
                }
                else
                {
                    delete_menu?.isVisible = false
                    edit_menu?.isVisible = false
                }
            }
        })



        goalsList.setHasFixedSize(true)
        goalsList.layoutManager = GridLayoutManager(this, 1)
}


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (outState != null){
            selectionTracker?.onRestoreInstanceState(outState)
        }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.list_actions_default, menu)
        this.edit_menu = menu?.findItem(R.id.menu_edit)
        this.delete_menu = menu?.findItem(R.id.menu_delete)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: android.view.MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_delete -> {
            return true}
    }
        return super.onOptionsItemSelected(item)
    }

//    fun deleteItem(): Boolean{
//    deleteTranction(this, adapter.getSelectedTransaction()!!)
//        adapter.list.removeAt(adapter.getRemovedTransactionPositon())
//        adapter.notifyDataSetChanged()
//        selectionTracker?.clearSelection()
//        return true
//
//    }




}
