package development.ugly.a001_app.settings_app

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import development.ugly.a001_app.R
import development.ugly.a001_app.customAdapters.CatRecAdapter
import development.ugly.a001_app.customDialogs.Category_Defn_Custom
import development.ugly.a001_app.database.Category_table.Category_table

import development.ugly.a001_app.database.DB_Helper
import kotlinx.android.synthetic.main.categories_list_activity.*

class CreateCategory : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var catTable = Category_table(this)
        setContentView(R.layout.categories_list_activity)

        Category_List_Rec.layoutManager = LinearLayoutManager(this)
        Category_List_Rec.adapter = CatRecAdapter(this,catTable.loadcategories(),{ account_fin -> Toast.makeText(this, "Clicked ",Toast.LENGTH_LONG).show()})

    }


    fun createCategory(view: View){
        Category_Defn_Custom().show(supportFragmentManager,"Category")
    }

}


