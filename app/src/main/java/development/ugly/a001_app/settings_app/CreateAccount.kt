package development.ugly.a001_app.settings_app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import development.ugly.a001_app.R
import development.ugly.a001_app.customAdapters.AcctRecAdapter
import development.ugly.a001_app.customDialogs.Acct_Defn_Custom
import development.ugly.a001_app.database.Accounts_table.loadAccounts
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import kotlinx.android.synthetic.main.activity_account_create.*

class CreateAccount : AppCompatActivity() {

var account_fin = Account_fin()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_account_create)

        Accounts_list_rec.layoutManager = LinearLayoutManager(this)
        Accounts_list_rec.adapter = AcctRecAdapter(this, loadAccounts(this), { account_fin -> Toast.makeText(this,account_fin.accot_type,Toast.LENGTH_LONG).show()})
    }

    fun CreateAccount(view: View){
        Acct_Defn_Custom().show(supportFragmentManager, "Date")
    }




}
