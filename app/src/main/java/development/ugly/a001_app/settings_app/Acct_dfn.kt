package development.ugly.a001_app.settings_app

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import development.ugly.a001_app.R
import development.ugly.a001_app.database.Accounts_table.createAccount
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import kotlinx.android.synthetic.main.bank_acct_frag.*


class Acct_dfn : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bank_acct_frag, container, false)
    }


}
