package development.ugly.a001_app.animations

import android.content.Context
import android.view.animation.Animation
import android.view.animation.AnimationUtils

class open_fadded(var context: Context){

    var isOpen: Boolean = false
    lateinit var open : Animation
    lateinit var close : Animation

  fun startAnimation(openAnim : Int, closeAnim: Int){
    open = AnimationUtils.loadAnimation(context, openAnim)
    close = AnimationUtils.loadAnimation(context, closeAnim)
   }


}