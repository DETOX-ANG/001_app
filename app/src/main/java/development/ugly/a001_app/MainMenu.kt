package development.ugly.a001_app

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import development.ugly.a001_app.customDialogs.Acct_Defn_Custom
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.settings_app.Acct_dfn
import development.ugly.a001_app.settings_app.CreateAccount
import development.ugly.a001_app.settings_app.CreateCategory
import kotlinx.android.synthetic.main.menu_list_content.*

class MainMenu : AppCompatActivity() {

     var isOpen: Boolean = false
    lateinit var openMenu :Animation
    lateinit var closeMenu :Animation


@RequiresApi(Build.VERSION_CODES.O)
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    setContentView(R.layout.activity_main_menu)

    startMenuAnimation()

    var transactionDb = Transaction_DB(this)

    var ss = transactionDb.loadInstallmentsByMonthCriteria("2019/05")

    }
    override fun onBackPressed() {
    }

    fun menuAnim(view: View){
        if (isOpen) {menuList.startAnimation(closeMenu)
        isOpen = false}
        else{menuList.startAnimation(openMenu)
        isOpen = true}
    }

    fun startMenuAnimation(){
        openMenu = AnimationUtils.loadAnimation(this, R.anim.menu_open)
        closeMenu = AnimationUtils.loadAnimation(this, R.anim.menu_close)
    }

 fun addButtonClick(view: View){
     var intent = Intent(this,CreateTransaction::class.java)
     startActivity(intent)
 }
 fun settingsButtonClick(view: View){
    Acct_Defn_Custom().show(supportFragmentManager,"Eu")
 }

fun CategoryButtonClick(view: View){
        var intent = Intent(this,CreateCategory::class.java)
        startActivity(intent)
    }

fun TransListButtonClick(view: View){
        var intent = Intent(this,TransactionList::class.java)
        startActivity(intent)
    }

}
