package development.ugly.a001_app.cls_Interfaces

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import development.ugly.a001_app.main_classes.Transaction_Fin

interface IAuxiliary {

    fun onCheckBoxVisibilityChange(checkBoxId: CheckBox, view: View) {
        if (checkBoxId.isChecked) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    //Make it generic
    fun <T> EditText.onTextChanged(cls : T){
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                Log.d("Eu","Eu")
            }

            override fun afterTextChanged(s: Editable?) {

                s.toString()

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("Eu","Eu")
            }
        })

    }
}