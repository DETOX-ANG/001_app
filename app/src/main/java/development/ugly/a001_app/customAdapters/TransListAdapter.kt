package development.ugly.a001_app.customAdapters

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.view.menu.MenuView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import development.ugly.a001_app.R.*
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.main_classes.Transaction_Fin
import kotlinx.android.synthetic.main.balance_card.view.*
import java.io.Serializable


class TransListAdapter (val context: Context?, val list: ArrayList<Transaction_Fin> ): RecyclerView.Adapter<RecViewHolder>(){

    var selectionTracker: SelectionTracker<Long>? = null
    var selectedTransacion: Transaction_Fin? = null
    private var removedTransactionPosition: Int = 0
    var value: Long = 0

    lateinit var itemClickListener: View.OnClickListener

    init {
        setHasStableIds(true)
    }


   constructor(context: Context?, list: ArrayList<Transaction_Fin>, clickListener: (Transaction_Fin) -> Unit ) : this(context, list){

//       this.itemClickListener = clickListener as View.OnClickListener
   }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecViewHolder {
       val viewholderInflate = LayoutInflater.from(context).inflate(layout.balance_card,p0,false)

        return  RecViewHolder(viewholderInflate)
    }

    override fun onBindViewHolder(p0: RecViewHolder, p1: Int) {

    selectionTracker.let { p0.bindView(list[p1], context, p1, it?.isSelected(p1.toLong())!!) }

    if (selectionTracker!!.isSelected(p1.toLong()))
    {
        p0.checkBox.visibility = View.VISIBLE
        setSelectedTransaction(list[p1])
        removedTransactionPosition = p1
//        Toast.makeText(context, selectedTransacion!!.trans_fin_id.toString(), Toast.LENGTH_LONG).show()


    }
        else
    {
        p0.checkBox.visibility = View.GONE
        }
    }

    override fun getItemCount() = list.size

    override fun getItemId(position: Int): Long {return position.toLong()}

    fun getRemovedTransactionPositon(): Int{
        return this.removedTransactionPosition
    }

    fun getSelectedTransaction(): Transaction_Fin?{
     return this.selectedTransacion
    }

    fun setSelectedTransaction(transactionFin: Transaction_Fin){
        this.selectedTransacion = transactionFin
    }
}



