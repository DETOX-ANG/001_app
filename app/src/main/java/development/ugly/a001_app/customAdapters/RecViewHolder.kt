package development.ugly.a001_app.customAdapters
import android.content.Context
import kotlinx.android.synthetic.main.balance_card.view.*


import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import androidx.recyclerview.selection.ItemDetailsLookup
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.main_classes.Transaction_Fin

class RecViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    var desc = itemView.Card_Name_Det
    var value = itemView.Card_Value_Det
    var dateT = itemView.Card_Date_Det
    var checkBox = itemView.Card_Selection_Det

    fun bindView(account_fin: Account_fin,clickListener: (Account_fin) -> Unit){
        desc.text = account_fin.acct_bal.toString()
        value.text = account_fin.acct_bal.toString()

        itemView.setOnClickListener { clickListener(account_fin)}
    }

    fun bindView(categories: Categories,clickListener: (Categories) -> Unit){
        desc.text = categories.cat_id.toString()
        value.text = categories.cat_description
        itemView.setOnClickListener { clickListener(categories)}
    }


    fun bindView(transactionFin: Transaction_Fin, context: Context?, position: Int, isActivated: Boolean = false ){
        desc.text = transactionFin.transdescription
        dateT.text = transactionFin.trans_date
        value.text = transactionFin.amount.toString()
//        itemView.setOnClickListener {
//            Toast.makeText(context, "$position", Toast.LENGTH_LONG).show()
//        }
        itemView.isActivated = isActivated
        dateT.visibility = View.VISIBLE
    }

    fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
        object : ItemDetailsLookup.ItemDetails<Long>() {
            override fun getPosition(): Int = adapterPosition
            override fun getSelectionKey(): Long? = itemId
        }

}