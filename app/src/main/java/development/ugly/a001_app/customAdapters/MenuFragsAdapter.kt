package development.ugly.a001_app.customAdapters

import android.content.Context
import android.content.Intent
import android.database.DataSetObserver
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.RequiresApi
import android.support.v4.app.*
import android.view.ViewGroup
import android.widget.Toast
import development.ugly.a001_app.*
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.main_classes.Transaction_Fin
import java.io.Serializable
import java.text.SimpleDateFormat
import java.time.Month
import java.util.*
import java.util.Calendar.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MenuFragsAdapter(var context: Context,var fragmentManager: FragmentManager, var data: HashMap<Int ,List<Transaction_Fin>>, var  size: Int ,var header: Transaction_Fin?): FragmentPagerAdapter(fragmentManager) {

        var sdt = SimpleDateFormat("MM/yyyy")
        var dateList = HashMap<String, Int>()
        var transactionDb = Transaction_DB(context)


    @RequiresApi(Build.VERSION_CODES.O)
    override fun getItem(p0: Int): Fragment {

        var bundle = Bundle()
        var fragment = Transactions_List_frag()
        transactionDb.loadInstallmentsByMonthCriteria(transactionDb.loadMonth()[p0])
        bundle.putSerializable("AA",transactionDb.loadInstallmentsByMonthCriteria(transactionDb.loadMonth()[p0]) as Serializable)
        fragment.arguments = bundle

        return fragment
    }

    override fun getCount(): Int = transactionDb.loadMonth().count()


    override fun getPageTitle(position: Int): CharSequence? {
        var sdt = SimpleDateFormat("YYYY/MMM")
        var sdf = SimpleDateFormat("yyyy/MM")
        var stringDate = transactionDb.loadMonth()[position]
        var tabg: Date = sdf.parse( stringDate)
        sdt.applyPattern("MMM/yyyy")
        var tabv = sdt.format(tabg)


        return tabv
    }


    override fun notifyDataSetChanged() {
        super.notifyDataSetChanged()
    }


}