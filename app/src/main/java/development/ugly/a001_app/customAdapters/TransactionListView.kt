package development.ugly.a001_app.customAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import development.ugly.a001_app.R

import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.main_classes.Transaction_Fin

class TransactionListView(private val context: Context, val dataSource:ArrayList<Transaction_Fin>): BaseAdapter() {

    //Preciso entender que merda é essa
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    var rowView = inflater.inflate(R.layout.trans_list_item,parent,false)
        val transaction = getItem(position) as Transaction_Fin

        var transValDet = rowView.findViewById<TextView>(R.id.TransValDet)
        var transDescDet = rowView.findViewById<TextView>(R.id.TransDescDet)
        var transCateDet = rowView.findViewById<TextView>(R.id.TransCategoryDet)


        transValDet.text = transaction.amount.toString()
        transDescDet.text = transaction.transdescription
        transCateDet.text = transaction.category

        return rowView
    }

    override fun getItem(position: Int): Any {
        return dataSource.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}