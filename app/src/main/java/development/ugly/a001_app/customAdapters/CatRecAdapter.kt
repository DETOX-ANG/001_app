package development.ugly.a001_app.customAdapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import development.ugly.a001_app.R.*
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.main_classes.Transaction_Fin



class CatRecAdapter (val context: Context?,val categories: ArrayList<Categories>, val clickListener: (Categories) -> Unit ): RecyclerView.Adapter<RecViewHolder>(){



    lateinit var itemClickListener: View.OnClickListener



    fun setOnItemClick(itemClickListener: View.OnClickListener){
        this.itemClickListener = itemClickListener


    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecViewHolder {
       val viewholderInflate = LayoutInflater.from(context).inflate(layout.balance_card,p0,false)

        return  RecViewHolder(viewholderInflate)
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(p0: RecViewHolder, p1: Int) {
           p0.bindView(categories[p1],clickListener)
    }


}



