package development.ugly.a001_app.customAdapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import development.ugly.a001_app.R
import kotlinx.android.synthetic.main.expand_child_activity.view.*
import java.util.ArrayList

class ExpCustList(var context: Context, var dataHeader:Array<String>,var dataChild: HashMap<String, Array<String>>): BaseExpandableListAdapter() {

    private  lateinit var ChildTextView: TextView
    private lateinit var  HeaderTexView: TextView
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getGroup(groupPosition: Int): Any {
        return dataHeader[groupPosition]
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        var itemText = getGroup(groupPosition) as String
        if(convertView == null){
            convertView = inflater.inflate(R.layout.expand_header_activity,null)
        }
        HeaderTexView = convertView!!.findViewById(R.id.HeaderLBL)
        HeaderTexView.text = itemText

        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
       return dataChild.get(dataHeader.get(groupPosition))!!.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return dataChild[dataHeader.get(groupPosition)]!![childPosition]
        //return dataChild.get(dataHeader.get(groupPosition))!!.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var itemText = getChild(groupPosition,childPosition) as String

        var convertView = convertView
        if (convertView == null){
            convertView = inflater.inflate(R.layout.expand_child_activity, null)
        }
        ChildTextView = convertView!!.findViewById(R.id.Child_LBL)
        ChildTextView.text = itemText
        return  convertView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return dataHeader.size
    }
}