package development.ugly.a001_app
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import java.util.*
import android.view.inputmethod.InputMethodManager
import development.ugly.a001_app.customAdapters.AcctRecAdapter
import development.ugly.a001_app.main_classes.Transaction_Fin
import java.text.SimpleDateFormat

var calendar = Calendar.getInstance()!!
var year = calendar.get(Calendar.YEAR)
var month = calendar.get(Calendar.MONTH)
var simpleDateFormat = SimpleDateFormat("MM/YYYY")
var myfilterMonth = "${month+1}/$year"
lateinit var bundle: Bundle
var date: Date = Date()

fun returnBoolean(): Boolean = true

fun AppCompatActivity.setFragment(fragment: Fragment, id: Int){
        var fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(id, fragment)
        fragmentTransaction.commit()

}


fun Fragment.setFragment(fragment: Fragment, id: Int, data: List<Transaction_Fin>){
    var fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
    fragmentTransaction.replace(id, fragment)
    fragmentTransaction.commit()
}

fun AppCompatActivity.removeFragment(fragment: Fragment){

    var fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.remove(fragment)
        fragmentTransaction.commit()
}


fun onCheckBoxVisibilityChange(checkBoxId: CheckBox, layoutId: View) {
   if (checkBoxId.isChecked) {
        layoutId.visibility = View.VISIBLE
        } else {
        layoutId.visibility = View.GONE
        }
    }

/*
fun loadCategories(context: Context?): ArrayAdapter<Categories> {
    var dB_Helper = DB_Helper(context)
    var arrayList: ArrayList<Categories> = ArrayList()
    var arrayAdapter: ArrayAdapter<Categories> =
        ArrayAdapter(context, R.layout.support_simple_spinner_dropdown_item, dB_Helper.getCategories(arrayList))
    return arrayAdapter
}*/

fun datePick (context: Context?, textView: TextView): Boolean {
var calendar = Calendar.getInstance()
    var sday = calendar.get(Calendar.DAY_OF_MONTH)
    var smonth = calendar.get(Calendar.MONTH)
    var syear = calendar.get(Calendar.YEAR)
    var date: SimpleDateFormat = SimpleDateFormat("DD/MM/YYYY")
    var listener =DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
        sday = dayOfMonth
        smonth = month +1
        syear = year
        textView.text = "$sday/$smonth/$syear"

     }
    var dialog =DatePickerDialog(context,listener,syear,smonth,sday)
    dialog.show()
    return  true
}

fun hideKeyboard(context: Context?, view: View): Boolean {
    view.requestFocus()
if(view != null)
{val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken,0)}
return true
}

fun filterRecListDate(textView: TextView, acctRecAdapter: AcctRecAdapter){
var std = SimpleDateFormat("MM/YYYY")


}


fun MonYearFilterWheel(monthFilter: NumberPicker, yearFilter: NumberPicker){
    monthFilter.wrapSelectorWheel = true
    monthFilter.maxValue = 12
    monthFilter.minValue = 1
    monthFilter.value = month + 1
    monthFilter.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

    yearFilter.wrapSelectorWheel = false
    yearFilter.maxValue = year + 50
    yearFilter.minValue = year - 50
    yearFilter.value = year
    yearFilter.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
}




/*
fun datepickMotionValidation(context: Context ,event: MotionEvent, textView: TextView): ArrayList<Int> {
    return when (event.action) {
        MotionEvent.ACTION_DOWN -> {
            hideKeyboard(context, textView)
            return datePick(context, textView)
        }
        else -> hideKeyboard(context, textView)
    }
}*/

