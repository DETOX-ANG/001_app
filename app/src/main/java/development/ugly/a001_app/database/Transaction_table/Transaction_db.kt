@file:Suppress("DEPRECATION")

package development.ugly.a001_app.database.Transaction_table

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import development.ugly.a001_app.*
import development.ugly.a001_app.database.Accounts_table.acct_col_desc
import development.ugly.a001_app.database.Accounts_table.acct_col_id
import development.ugly.a001_app.database.Accounts_table.acct_tbl_name
import development.ugly.a001_app.database.Category_table.Category_table
import development.ugly.a001_app.database.DB_Helper
import development.ugly.a001_app.main_classes.Transaction_Fin
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Transaction_DB (){

    constructor(context_ : Context?) : this() {
        this.context = context_
    }

    var context: Context? = null
    var ftrans_adv_table_name = "Transaction_fin_det"

    private var cat_table : Category_table = Category_table()

    private var trans_col_id = "ID"
    private var trans_col_desc = "DESCRIPTION"
    private var dueDate = "DUE_DATE"
    private var trans_cat_col_ID = "CAT_ID"
    private var ftrans_col_amount: String = "AMOUNT_VALUE"
    private var trans_acct_col_id = "ACCT_ID"
    private var trans_col_dist_amt_style = "DIST_AMT_STYLE"
    private var trans_col_date = "TRANS_DATE"
    private var status = "PYMNT_STATUS"
    private var payDate = "PYMNT_DATE"
    private var installmentsCount = "INSTALL_SEQ_NUM"
    lateinit  var dB_Helper: SQLiteDatabase
//    private var lIntallments: String =
//        "Select D.$ftrans_col_ID,D.$ftrans_col_desc, A.$ftrans_col_amount, A.$cat_col_ID , B.$cat_col_desc, C.$acct_col_id ,C.$acct_col_desc, A.$dueDate, A.$installmentsCount" + " from $ftrans_adv_table_name A, $category_table_name B, $acct_tbl_name C, $ftrans_table_name D " + "where D.$ftrans_col_ID = A.$trans_col_id and B.$cat_col_ID = A.$cat_col_ID and C.$acct_col_id = A.$acct_col_id" + "\nUNION\n" + "Select A.$trans_col_id, A.$ftrans_col_desc, A.$ftrans_col_amount,A.$cat_col_ID ,B.$cat_col_desc, C.$acct_col_id ,C.$acct_col_desc, A.$ftrans_col_date, A.$ftrans_col_rpt_number " + "from $ftrans_table_name A, $category_table_name B, $acct_tbl_name C " + "where B.$cat_col_ID = A.$cat_col_ID and C.$acct_col_id = A.$acct_col_id and A.$ftrans_col_rpt_number = 1"

    var transactionFinTable =
        "CREATE TABLE $ftrans_adv_table_name ($trans_col_id INTEGER, " + "$trans_col_desc TEXT NOT NULL," + "$dueDate REAL NOT NULL," + "$trans_cat_col_ID INTEGER, " + "$trans_acct_col_id INTEGER, " + "$trans_col_dist_amt_style TEXT, " + "$trans_col_date REAL, " + "$ftrans_col_amount REAL NOT NULL, " + "$status TEXT, " + "$payDate REAL," + "$installmentsCount INTEGER, " + "FOREIGN KEY(${cat_table.cat_col_ID}) REFERENCES ${cat_table.category_table_name}(${cat_table.cat_col_ID})," + "FOREIGN KEY($acct_col_id) REFERENCES $acct_tbl_name($acct_col_id))"

    var groupMonthSql = "SELECT (substr($dueDate,7,4)||'/'||substr($dueDate,4,2)) as $dueDate FROM 'Transaction_fin_det' group by (substr($dueDate,7,4)||'/'||substr($dueDate,4,2)) order by date((substr($dueDate,7,4)||'/'||substr($dueDate,4,2))) desc"



    fun saveTransDetails(transactions: ArrayList<Transaction_Fin>) {
        for (transaction_Fin in transactions) {
            dB_Helper = DB_Helper(context).writableDatabase
            var contentValues = ContentValues()
            contentValues.put(trans_col_id, transaction_Fin.trans_fin_id)
            contentValues.put(trans_col_desc, transaction_Fin.transdescription)
            contentValues.put(trans_acct_col_id, transaction_Fin.account_fin.acct_id)
            contentValues.put(dueDate, transaction_Fin.trans_date)
            contentValues.put(trans_cat_col_ID, transaction_Fin.categories.cat_id)
            contentValues.put(ftrans_col_amount, transaction_Fin.amount)
            contentValues.put(installmentsCount, transaction_Fin.installSeq)
            contentValues.put(status, "N")
            contentValues.put(payDate, "N")
            dB_Helper.insert(ftrans_adv_table_name, null, contentValues)
            dB_Helper.close()
        }
    }

    fun loadInstallments(): ArrayList<Transaction_Fin> {
        dB_Helper = DB_Helper(context).writableDatabase
        var date = SimpleDateFormat("DD/MM/YYYY")
        var arrayList = ArrayList<Transaction_Fin>()
        var cursor = dB_Helper.rawQuery(
            "Select A.$trans_col_id,A.$trans_col_desc, A.$ftrans_col_amount, A.${cat_table.cat_col_ID} , B.${cat_table.cat_col_desc}, C.$acct_col_id ,C.$acct_col_desc, A.$dueDate, A.$installmentsCount" + " from $ftrans_adv_table_name A, ${cat_table.category_table_name} B, $acct_tbl_name C " + "where B.${cat_table.cat_col_ID} = A.${cat_table.cat_col_ID} and C.$acct_col_id = A.$acct_col_id ",
            null
        )
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(
                    Transaction_Fin(
                        cursor.getInt(cursor.getColumnIndex(trans_col_id)),
                        cursor.getString(cursor.getColumnIndex(trans_col_desc)),
                        cursor.getDouble(cursor.getColumnIndex(ftrans_col_amount)),
                        cursor.getString(cursor.getColumnIndex(Category_table().cat_col_desc)),
                        cursor.getString(cursor.getColumnIndex(acct_col_desc)),
                        cursor.getString(cursor.getColumnIndex(dueDate)),
                        cursor.getInt(cursor.getColumnIndex(installmentsCount))
                    )
                )
            } while (cursor.moveToNext())
        }
        return arrayList
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(Build.VERSION_CODES.O)
    fun loadInstallmentsByMonthCriteria(month: String): ArrayList<Transaction_Fin> {
        dB_Helper = DB_Helper(context).writableDatabase

        var convertedDate = (month.substring(5,7)+"/"+month.substring(0,4))
        var loadInstallments ="Select A.$trans_col_id,A.$trans_col_desc, A.$ftrans_col_amount, A.${cat_table.cat_col_ID} , B.${cat_table.cat_col_desc}, C.$acct_col_id ,C.$acct_col_desc, A.$dueDate, A.$installmentsCount" + " from $ftrans_adv_table_name A, ${cat_table.category_table_name} B, $acct_tbl_name C " + "where B.${cat_table.cat_col_ID} = A.${cat_table.cat_col_ID} and C.$acct_col_id = A.$acct_col_id and ${dueDate} like '%$convertedDate%'"

        var arrayList = ArrayList<Transaction_Fin>()
        var cursor = dB_Helper.rawQuery(loadInstallments,null
        )
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(
                    Transaction_Fin(
                        cursor.getInt(cursor.getColumnIndex(trans_col_id)),
                        cursor.getString(cursor.getColumnIndex(trans_col_desc)),
                        cursor.getDouble(cursor.getColumnIndex(ftrans_col_amount)),
                        cursor.getString(cursor.getColumnIndex(Category_table().cat_col_desc)),
                        cursor.getString(cursor.getColumnIndex(acct_col_desc)),
                        cursor.getString(cursor.getColumnIndex(dueDate)),
                        cursor.getInt(cursor.getColumnIndex(installmentsCount))
                    )
                )
            } while (cursor.moveToNext())
        }
        cursor.close()
        dB_Helper.close()
        return arrayList
    }



    fun loadMonth(): ArrayList<String>{
        dB_Helper = DB_Helper(context).writableDatabase
        var arrayList = ArrayList<String>()
        var date = SimpleDateFormat("MM/YYYY")


        var cursor = dB_Helper.rawQuery(groupMonthSql,null)


        if (cursor.moveToFirst()){
            do {
                arrayList.add(cursor.getString(cursor.getColumnIndex(dueDate)))

            }
                while (cursor.moveToNext())
        }
        cursor.close()
        dB_Helper.close()

        return arrayList
    }

    fun countMonths(): Int {
        return dB_Helper.rawQuery(groupMonthSql, null).count
    }


    fun groupInstallmentsByDate2(arrayList: ArrayList<Transaction_Fin>): HashMap<Int, List<Transaction_Fin>> {
        var sdt = SimpleDateFormat("MM/yyyy")
        var dateGroup: HashMap<Int, List<Transaction_Fin>> = HashMap()
        var dateGroup2: HashMap<Int, List<Transaction_Fin>> = HashMap()
        var min = arrayList.minBy { fin -> fin.sdate.time }?.sdate
        var position: Int = 0
        do {
            var filt = arrayList.filter { transactionFin -> transactionFin.trans_date.contains(sdt.format(min?.time)) }
            dateGroup.put(position, filt)
            min?.add(Calendar.MONTH, 1)
            position++
        } while (position != arrayList.size)

        var finalMap = reindexMap(removeEmptyMaps(dateGroup))

    return finalMap
    }

    fun groupInstallmentsByDate3(): HashMap<Int, List<Transaction_Fin>> {
        var sdt = SimpleDateFormat("MM/yyyy")
        var arrayList = loadInstallments()
        var dateGroup: HashMap<Int, List<Transaction_Fin>> = HashMap()

        var min = arrayList.minBy { fin -> fin.sdate.time }?.sdate
        var position: Int = 0
        do {
            var filt = arrayList.filter { transactionFin -> transactionFin.trans_date.contains(sdt.format(min?.time)) }
            dateGroup.put(position, filt)
            min?.add(Calendar.MONTH, 1)
            position++
        } while (position != arrayList.size)

        var finalMap = reindexMap(removeEmptyMaps(dateGroup))


        return finalMap
    }

    fun groupDateByString(mindate: String?, ListSize: Int): HashMap<String, Int> {
        var sdt = SimpleDateFormat("MM/yyyy")
        var sdf = SimpleDateFormat("dd/MM/yyyy")
        var dateGroup: HashMap<String, Int> = HashMap()
        var min = sdf.parse(mindate)
        var calendar = Calendar.getInstance()
        calendar.time = min
        var maxSeq = ListSize
        var position: Int = 0
        do {
            dateGroup.put(sdt.format(calendar?.time), position)
            calendar?.add(Calendar.MONTH, 1)
            position++
        } while (position != maxSeq)
        return dateGroup
    }

    fun group2DateByString(hashMap: HashMap<Int, List<Transaction_Fin>>): HashMap<String, Int> {
        var sdt = SimpleDateFormat("MM/yyyy")
        var sdf = SimpleDateFormat("dd/MM/yyyy")
        var dateGroup: HashMap<String, Int> = HashMap()
        var position: Int = 0

        for (item in hashMap) {
            dateGroup.put(sdt.format(item.value[0].sdate.time), position)
            position++
        }

        return dateGroup
    }

    fun retMinDate(arrayList: ArrayList<Transaction_Fin>): Transaction_Fin? {
        var min = arrayList.minBy { fin -> fin.sdate.time }
        return min
    }

    fun deleteTranction(transactionFin: Transaction_Fin): Int {
        dB_Helper = DB_Helper(context).writableDatabase
        // Deletar transação atual

        var returnVal = dB_Helper.delete(
            ftrans_adv_table_name,
            "$trans_col_id = ${transactionFin.trans_fin_id} and $installmentsCount = ${transactionFin.installSeq} ",
            null
        )
        return returnVal
    }

    fun deleteFullTranction(transactionFin: Transaction_Fin): Int {
        dB_Helper = DB_Helper(context).writableDatabase

        var returnVal = dB_Helper.delete(
            ftrans_adv_table_name,
            "$trans_col_id = ${transactionFin.trans_fin_id}",
            null
        )
        return returnVal
    }

    fun deleteFutureTranction(transactionFin: Transaction_Fin): Int {
        dB_Helper = DB_Helper(context).writableDatabase

        var returnVal = dB_Helper.delete(
            ftrans_adv_table_name,
            "$trans_col_id = ${transactionFin.trans_fin_id} and $installmentsCount >= ${transactionFin.installSeq} ",
            null
        )
        return returnVal
    }

    fun removeEmptyMaps(hashMap: HashMap<Int, List<Transaction_Fin>>): HashMap<Int, List<Transaction_Fin>> {

        return hashMap.filter { entry -> entry.value.size != 0 } as HashMap<Int, List<Transaction_Fin>>
    }

    fun reindexMap(hashMap: HashMap<Int, List<Transaction_Fin>>): HashMap<Int, List<Transaction_Fin>> {
        var position = 0
        var newMap = HashMap<Int, List<Transaction_Fin>>()
        for (item in hashMap) {
            newMap.put(position, item.value)
            position++
        }
        return newMap
    }

    fun countInstllments(id: Int?): Int {
        var sql = "SELECT COUNT($trans_col_id) as $trans_col_id FROM $ftrans_adv_table_name WHERE $trans_col_id = $id"
        dB_Helper = DB_Helper(context).writableDatabase
        var cursor =dB_Helper.rawQuery(sql,null)

        cursor.moveToFirst()
        return cursor.getInt(cursor.getColumnIndex(trans_col_id))
    }

    fun returnNextValue(): Int? {
        var transaction_Fin = Transaction_Fin()
        dB_Helper = DB_Helper(context).writableDatabase
        var cursor = dB_Helper.rawQuery("SELECT MAX($trans_col_id) AS $trans_col_id FROM $ftrans_adv_table_name",null)
        if (cursor.moveToFirst()){
            do {transaction_Fin.trans_fin_id = cursor.getInt(cursor.getColumnIndex(trans_col_id))+1}
            while (cursor.moveToNext())}
        return transaction_Fin.trans_fin_id}

}
