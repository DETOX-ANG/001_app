package development.ugly.a001_app.database.Category_table

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.ArrayAdapter
import android.widget.Toast
import development.ugly.a001_app.database.DB_Helper
import development.ugly.a001_app.main_classes.Categories
import development.ugly.a001_app.settings_app.CreateCategory
import java.lang.NullPointerException

class Category_table() {

    constructor(context_: Context?) : this() {
        this.context = context_

    }
    var context : Context? = null
    lateinit var db: SQLiteDatabase
    var category_table_name = "Category_fin_tbl"
    var cat_col_ID = "Cat_ID"
    var cat_col_desc: String = "cat_description"
    var cat_col_parent: String = "cat_parent"

    //CREATE DB SQL
    val categoryTable =
        "CREATE TABLE $category_table_name ($cat_col_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE," + " $cat_col_desc TEXT UNIQUE," + " $cat_col_parent TEXT)"


    fun loadcategories(): ArrayList<Categories> {
        db = DB_Helper(context).writableDatabase
        var arrayList: ArrayList<Categories> = ArrayList()
        var cursor: Cursor = db.rawQuery("SELECT * FROM $category_table_name", null)
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(
                    Categories(
                        cursor.getInt(cursor.getColumnIndex(cat_col_ID)),
                        cursor.getString(cursor.getColumnIndex(cat_col_desc)),
                        cursor.getString(cursor.getColumnIndex(cat_col_parent))
                    )
                )
            } while (cursor.moveToNext())
        }
        return arrayList
    }

    fun InsertCategory(categories: Categories): Boolean {
        db = DB_Helper(context).writableDatabase
        var contentValues = ContentValues()
        contentValues.put(cat_col_desc, categories.cat_description)
        contentValues.put("cat_parent", categories.cat_parent)
        var result = db.insert(category_table_name, null, contentValues)
        if (result == -1.toLong()) {
            Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show()
            return true
        } else {
            Toast.makeText(context, "Success", Toast.LENGTH_LONG).show()
            return false
        }
    }
}