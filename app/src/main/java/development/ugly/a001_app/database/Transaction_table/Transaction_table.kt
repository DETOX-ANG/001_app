//package development.ugly.a001_app.database.Transaction_table
//
//import android.content.ContentValues
//import android.content.Context
//import android.database.sqlite.SQLiteDatabase
//import android.widget.Toast
//import development.ugly.a001_app.CreateTransaction
//import development.ugly.a001_app.Distribution_Style
//import development.ugly.a001_app.TransactionList
//import development.ugly.a001_app.database.Accounts_table.acct_col_desc
//import development.ugly.a001_app.database.Accounts_table.acct_col_id
//import development.ugly.a001_app.database.Accounts_table.acct_tbl_name
//import development.ugly.a001_app.database.Category_table.cat_col_ID
//import development.ugly.a001_app.database.Category_table.cat_col_desc
//import development.ugly.a001_app.database.Category_table.category_table_name
//import development.ugly.a001_app.database.Category_table.db
//import development.ugly.a001_app.database.DB_Helper
//import development.ugly.a001_app.main_classes.Transaction_Fin
//
//lateinit  var dB: SQLiteDatabase
//
//var ftrans_table_name = "Transaction_fin_wrk"
//var ftrans_col_ID = "TRANSACTION_ID"
//var ftrans_col_desc = "DESCRIPTION"
//var fatrans_col_amount: String = "AMOUNT_VALUE"
//var ftrans_col_date = "TRANS_DATE"
//var ftrans_col_paid_amt = "PAID_AMT"
//
//var ftrans_col_repeat = "REPEAT_TRANS"
//var ftrans_col_dist_amt_style = "DIST_AMT_STYLE"
//var ftrans_col_rpt_number = "REPEAT_NUMBER"
//var ftrans_col_account = "ACCT_ID"
//var ftrans_col_rpt_start = "REPEAT_START"
//var ftrans_col_rpt_end = "REPEAT_END"
///*
//var transactionTable = "CREATE TABLE $ftrans_table_name ($ftrans_col_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
//                                                        "$ftrans_col_desc TEXT NOT NULL" +
//                                                        ", $ftrans_col_amount REAL NOT NULL, " +
//                                                        "$ftrans_col_date NUMERIC NOT NULL," +
//                                                        " $cat_col_desc TEXT," +
//                                                        " $acct_col_desc TEXT, " +
//                                                        "$ftrans_col_repeat BLOB, " +
//                                                        "$ftrans_col_dist_amt_style TEXT, " +
//                                                        "$ftrans_col_rpt_start INTEGER," +
//                                                        " $ftrans_col_rpt_end INTEGER, " +
//                                                         "FOREIGN KEY($cat_col_desc) REFERENCES $category_table_name($cat_col_desc)," +
//                                                        "FOREIGN KEY($acct_col_desc) REFERENCES $acct_tbl_name($acct_col_desc))"*/
//
//
//var transactionTable = "CREATE TABLE $ftrans_table_name ($ftrans_col_ID INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
//        "$ftrans_col_desc TEXT NOT NULL" +
//        ", $ftrans_col_amount REAL NOT NULL, " +
//        "$ftrans_col_date REAL NOT NULL," +
//        " $cat_col_ID INTEGER," +
//        " $acct_col_id INTEGER," +
//        "$ftrans_col_repeat BLOB, " +
//        "$ftrans_col_dist_amt_style TEXT, " +
//       " $ftrans_col_rpt_number INTEGER," +
//        " $ftrans_col_paid_amt REAL," +
//        "FOREIGN KEY($cat_col_ID) REFERENCES $category_table_name($cat_col_ID)," +
//        "FOREIGN KEY($acct_col_id) REFERENCES $acct_tbl_name($acct_col_id))"
//
//
//
///*
//TODO  simplify Method remove excess lines
//TODO create better messages to return
//*/
//fun CreateTransaction.saveTransaction(context: Context, transaction_Fin: Transaction_Fin){
//    db = DB_Helper(context).writableDatabase
//    var contentValues = ContentValues()
//    contentValues.put(ftrans_col_ID, transaction_Fin.trans_fin_id)
//    contentValues.put(ftrans_col_desc,transaction_Fin.transdescription)
////    contentValues.put(ftrans_col_amount,transaction_Fin.amount)
//    contentValues.put(ftrans_col_date,transaction_Fin.trans_date)
//    contentValues.put(cat_col_ID, transaction_Fin.categories.cat_id)
//    contentValues.put(acct_col_id, transaction_Fin.account_fin.acct_id)
//    contentValues.put(ftrans_col_repeat, transaction_Fin.installments)
//    contentValues.put(ftrans_col_dist_amt_style, transaction_Fin.distribution_Style.toString())
//    contentValues.put(ftrans_col_rpt_number,transaction_Fin.installSeq)
//    var result = db.insert(ftrans_table_name, null,contentValues)
//    if (result == -1.toLong())
//        Toast.makeText(context,"Failed", Toast.LENGTH_LONG).show()
//    else
//        Toast.makeText(context,"Success", Toast.LENGTH_LONG).show()
//}
//
//
//fun loadTransactions(context: Context):ArrayList<Transaction_Fin>{
//    db = DB_Helper(context).writableDatabase
//    var arrayList = ArrayList<Transaction_Fin>()
//    var cursor = db.rawQuery("Select A.$ftrans_col_desc, A.$ftrans_col_amount,A.$cat_col_ID ,B.$cat_col_desc, C.$acct_col_id ,C.$acct_col_desc, A.$ftrans_col_date from $ftrans_table_name A, $category_table_name B, $acct_tbl_name C where B.$cat_col_ID = A.$cat_col_ID and C.$acct_col_id = A.$acct_col_id",null)
//    if (cursor.moveToFirst()){
//     do {
//            arrayList.add(Transaction_Fin(cursor.getString(cursor.getColumnIndex(ftrans_col_desc)),cursor.getDouble(cursor.getColumnIndex(
//                ftrans_col_amount)),cursor.getString(cursor.getColumnIndex(cat_col_desc)),cursor.getString(cursor.getColumnIndex(
//                acct_col_desc)),cursor.getString(cursor.getColumnIndex(ftrans_col_date))))
//        }
//    while (cursor.moveToNext())
//    }
//    return arrayList
//}
//
//fun CreateTransaction.returnLastTransID(context: Context): Int?{
//    var transaction_Fin = Transaction_Fin()
//    db = DB_Helper(context).writableDatabase
//    var cursor = db.rawQuery("SELECT MAX($ftrans_col_ID) AS $ftrans_col_ID FROM $ftrans_table_name",null)
//    if (cursor.moveToFirst()){
//        do {
//            transaction_Fin.trans_fin_id = cursor.getInt(cursor.getColumnIndex(ftrans_col_ID))
//        }
//        while (cursor.moveToNext())
//    }
//    return transaction_Fin.trans_fin_id
//}