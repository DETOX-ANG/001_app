package development.ugly.a001_app.database.Accounts_table

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import development.ugly.a001_app.main_classes.Accounts.Bank_account


var bk_acct_table_name = "bk_acct_tbl"
var bk_col_id = "Bank_id"
var bk_col_Bk_Name = "Bank_name"
var bk_col_Initial_balance = "Bank_init_bal"
var bk_col_Bank_Acct_category = "Bank_acct_category"


val bankAcctTable: String = "CREATE TABLE $bk_acct_table_name ($acct_col_id INTEGER UNIQUE, " +
                                         "$bk_col_Initial_balance REAL, " +
                                          "$bk_col_Bank_Acct_category TEXT, " +
                                          "FOREIGN KEY($acct_col_id) REFERENCES $acct_tbl_name($acct_col_id))"


fun SQLiteOpenHelper.insertBankAccount(
    bank_account: Bank_account,
    context: Context
){
    val db = writableDatabase
    var contentValues = ContentValues()
    contentValues.put(acct_col_id,bank_account.acct_id)
    contentValues.put(bk_col_Initial_balance, bank_account.acct_init_bal)
    contentValues.put(bk_col_Bank_Acct_category, bank_account.account_type)
    try {
        var result =db.insertOrThrow(bk_acct_table_name, null,contentValues)
    }
    catch (e: SQLiteException){
        Toast.makeText(context,e.message, Toast.LENGTH_LONG).show()
    }


}



