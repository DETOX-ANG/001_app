package development.ugly.a001_app.database.Accounts_table

import android.content.ContentValues
import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import development.ugly.a001_app.customDialogs.Acct_Defn_Custom
import development.ugly.a001_app.settings_app.CreateAccount
import development.ugly.a001_app.database.DB_Helper
import development.ugly.a001_app.main_classes.Accounts.Account_fin
import development.ugly.a001_app.settings_app.Acct_dfn


lateinit var db: SQLiteDatabase

var acct_tbl_name = "ACCOUNT_TBL"
var acct_col_id = "ACCT_ID"
var acct_col_desc = "ACCT_DESC"
var acct_col_type = "ACCT_TYPE"
var acct_col_balance = "ACCT_BALANCE"
var contentValue = ContentValues()

 var acctTable = "CREATE TABLE $acct_tbl_name ($acct_col_id INTEGER PRIMARY KEY, $acct_col_desc TEXT UNIQUE NOT NULL, $acct_col_type TEXT, $acct_col_balance REAL)"

fun Acct_Defn_Custom.createAccount(context: Context?, account_fin: Account_fin): Boolean {
 var db = DB_Helper(context).writableDatabase
 contentValue.put(acct_col_id,account_fin.acct_id)
 contentValue.put(acct_col_desc, account_fin.acct_desc)
 contentValue.put(acct_col_type, account_fin.accot_type)
 contentValue.put(acct_col_balance, account_fin.acct_bal)
try {
  var result = db.insertOrThrow(acct_tbl_name, null,contentValue)
    return true
  }
 catch (e: SQLException){

     return false
 }
}


fun loadAccounts(context: Context?): ArrayList<Account_fin>{
    var db = DB_Helper(context).writableDatabase
    var arrayList : ArrayList<Account_fin> = ArrayList()
    var sql = "Select $acct_col_id, $acct_col_desc, $acct_col_balance, $acct_col_type from $acct_tbl_name"
    var cursor = db.rawQuery(sql, null)
    if (cursor.moveToFirst()){
        do{arrayList.add(
            Account_fin(cursor.getInt(cursor.getColumnIndex(acct_col_id)),cursor.getString(cursor.getColumnIndex(acct_col_desc)), cursor.getString(cursor.getColumnIndex(
            acct_col_type)), cursor.getDouble(cursor.getColumnIndex(acct_col_balance)))
        )
        }
        while (cursor.moveToNext())
    }
    return arrayList
}


