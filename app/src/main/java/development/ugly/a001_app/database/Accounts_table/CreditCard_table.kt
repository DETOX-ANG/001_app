package development.ugly.a001_app.database.Accounts_table

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import development.ugly.a001_app.main_classes.Accounts.CreditCard_Account


var CreditCard_table_name: String = "Cred_Card_tbl"
var CC_col_id = "CC_ID"
var CC_col_name = "CC_NAME"
var CC_col_limit = "Credit_limit"
var CC_col_close_day = "day_close"

var CCTable = "CREATE TABLE $CreditCard_table_name ($acct_col_id INTEGER UNIQUE, " +
                                                           "$CC_col_limit REAL, " +
                                                           "$CC_col_close_day INTEGER, " +
                                                            "FOREIGN KEY($acct_col_id) REFERENCES $acct_tbl_name($acct_col_id))"


fun SQLiteOpenHelper.insertCreditCard(context: Context, creditCard_Account: CreditCard_Account){
    var contentValues = ContentValues()
    val db = writableDatabase
    contentValues.put(acct_col_id, creditCard_Account.acct_id)
    contentValues.put(CC_col_limit, creditCard_Account.CC_Limit)
    contentValues.put(CC_col_close_day, creditCard_Account.CC_Close_day)
    var result = db.insert(CreditCard_table_name, null,contentValues)
    if (result == -1.toLong())
        Toast.makeText(context,"Failed", Toast.LENGTH_LONG).show()
    else
        Toast.makeText(context,"Success", Toast.LENGTH_LONG).show()
    }