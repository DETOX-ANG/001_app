package development.ugly.a001_app.recyclerView_custom

import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import development.ugly.a001_app.customAdapters.RecViewHolder

class ItemDetail(val recyclerView: RecyclerView) : ItemDetailsLookup<Long>() {

    override fun getItemDetails(p0: MotionEvent): ItemDetails<Long>? {

        val view = recyclerView.findChildViewUnder(p0.x, p0.y)
        if (view != null){
            return (recyclerView.getChildViewHolder(view) as RecViewHolder).getItemDetails()
        }
        return null
    }
}