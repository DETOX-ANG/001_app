package development.ugly.a001_app

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.Menu
import android.view.MenuItem
import development.ugly.a001_app.customAdapters.MenuFragsAdapter
import development.ugly.a001_app.database.Transaction_table.*


import development.ugly.a001_app.main_classes.Transaction_Fin
import kotlinx.android.synthetic.main.transaction_list_activity.*
import java.text.SimpleDateFormat
import java.util.*

class TransactionList : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    var delete_menu: MenuItem? = null
    var edit_menu: MenuItem? = null
    var sdf: SimpleDateFormat = SimpleDateFormat("MM/yyyy")
    lateinit var header: Transaction_Fin
    var cDate = Calendar.getInstance()
    lateinit var adapter: MenuFragsAdapter
    lateinit var db : Transaction_DB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_list_activity)

         db = Transaction_DB(this)



        var mont = db.loadMonth()
        var load =db.loadInstallments()
        var sizeLoad = load.maxBy { transactionFin -> transactionFin.installSeq }?.installSeq
        var dataMap = db.groupInstallmentsByDate2(load)
        var i =db.group2DateByString(dataMap)

        header = db.retMinDate(load) as Transaction_Fin
        tabLayout.setupWithViewPager(TransList_Pager)

        adapter = MenuFragsAdapter(this,this.supportFragmentManager,dataMap,sizeLoad as Int,header)

        TransList_Pager.adapter = adapter
        setSupportActionBar(transList_toolbar)


        TransList_Pager.currentItem = i[sdf.format(cDate.time)] as Int
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_actions_default, menu)
        this.edit_menu = menu?.findItem(R.id.menu_edit)
        this.delete_menu = menu?.findItem(R.id.menu_delete)

        return super.onCreateOptionsMenu(menu)
    }

}


