package development.ugly.a001_app

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.Toast
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StableIdKeyProvider
import androidx.recyclerview.selection.StorageStrategy
import development.ugly.a001_app.customAdapters.TransListAdapter
import development.ugly.a001_app.customDialogs.TransListDelDialog
import development.ugly.a001_app.database.Transaction_table.Transaction_DB
import development.ugly.a001_app.main_classes.Transaction_Fin
import development.ugly.a001_app.recyclerView_custom.ItemDetail
import kotlinx.android.synthetic.main.trans_list_del_dialog.*
import kotlinx.android.synthetic.main.transactions__list_frag.*
import java.io.Serializable
import kotlin.collections.ArrayList

//TODO Melhorar chamadas dessa classe
class Transactions_List_frag : Fragment() {


    lateinit var list: ArrayList<Transaction_Fin>
    var label: Transaction_Fin? = null
    lateinit var adapter : TransListAdapter
    var selectionTracker: SelectionTracker<Long>? = null
    lateinit var menuItem: Toolbar
    lateinit var aPagerAdapter: PagerAdapter
    lateinit var ItemToDel : Transaction_Fin

    var db: Transaction_DB = Transaction_DB(context)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.transactions__list_frag, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = Transaction_DB(context)
        menuItem = activity?.findViewById<Toolbar>(R.id.transList_toolbar) as Toolbar



        var installments = this.arguments!!["AA"] as ArrayList<Transaction_Fin>
        adapter = TransListAdapter(context, installments, {Toast.makeText(context,"item clicado", Toast.LENGTH_LONG).show()})


        ListTransaction.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        ListTransaction.layoutManager = LinearLayoutManager(context)
        setTextViewText(installments)

        ListTransaction.adapter =adapter

        selectionTracker = SelectionTracker.Builder("aa1",
            ListTransaction,
            StableIdKeyProvider(ListTransaction),
            ItemDetail(ListTransaction),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate( SelectionPredicates.createSelectSingleAnything()).build()

        adapter.selectionTracker = selectionTracker


        selectionTracker?.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                super.onSelectionChanged()
                var item = selectionTracker!!.selection.size()
                if (item > 0)
                {
                 setMenuItemsVisibility(true)
                 menuItem.setOnMenuItemClickListener { menuItem -> MenuActions(menuItem)
                }}
                else
                {
                 setMenuItemsVisibility(false)
                }}})
        }

    private fun setMenuItemsVisibility(status: Boolean) {
        menuItem.menu?.findItem(R.id.menu_delete)?.isVisible = status
        menuItem.menu?.findItem(R.id.menu_edit)?.isVisible = status
    }

    private fun MenuActions(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.menu_delete -> {
            deleteItem(adapter.getSelectedTransaction()!!, adapter.getRemovedTransactionPositon())
            }
            R.id.menu_edit -> {
            deleteItem(adapter.getSelectedTransaction()!!, adapter.getRemovedTransactionPositon())
            }
            else -> returnBoolean()
        }
    }

    fun deleteItem(transactionFin: Transaction_Fin, removeItemRec: Int): Boolean{
        if (db.countInstllments(transactionFin.trans_fin_id)>1){
            var bundle = Bundle()
            bundle.putSerializable("TRANSID", transactionFin)
            var transListDelDialog = TransListDelDialog()
            transListDelDialog.arguments = bundle
            transListDelDialog.show(fragmentManager, "DelDiag")
        }
        else
        {
        db.deleteTranction(transactionFin)
        adapter.list.removeAt(removeItemRec)
        adapter.notifyDataSetChanged()
        selectionTracker?.clearSelection()

        }
       return true
    }


    private fun setTextViewText(installments: ArrayList<Transaction_Fin>) {
        TotalListAmt_TXT.text = installments.sumByDouble { transactionFin -> transactionFin.amount }.toString()
        OutComeValue_LBL.text = installments.filter { transactionFin -> transactionFin.amount < 0 }.sumByDouble { transactionFin -> transactionFin.amount }.toString()
        IncomeValue_LBL.text = installments.filter { transactionFin -> transactionFin.amount > 0 }.sumByDouble { transactionFin -> transactionFin.amount }.toString()
    }
}

