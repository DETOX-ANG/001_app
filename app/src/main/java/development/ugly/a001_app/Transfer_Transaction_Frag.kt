package development.ugly.a001_app

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import development.ugly.a001_app.database.Accounts_table.loadAccounts

import kotlinx.android.synthetic.main.transfer__transaction_frag.*


class Transfer_Transaction_Frag : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transfer__transaction_frag, container, false)
    }

}
